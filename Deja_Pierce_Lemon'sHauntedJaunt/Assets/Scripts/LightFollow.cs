﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFollow : MonoBehaviour {

    public GameObject JohnLemon; //tells the light where to move to

    // Start is called before the first frame update
  
    // Update is called once per frame
    void Update()
    {
        transform.LookAt(JohnLemon.transform); //tells the camera where to look at
    }
}
